import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  RutaJsonHeader :string = "./assets/InformacionJson/Home.json";
  Informacion:any;
  Isdatos : boolean=false;
  constructor(private http: HttpClient)
 {
 }
  ngOnInit()
  {
    this.LeerInformacionJson();
  }

  LeerInformacionJson()
  {
    this.http.get(this.RutaJsonHeader).toPromise().then(data=> {
      this.Informacion = data;
      this.Isdatos = true;
     });
  }

}
