import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


 const routes: Routes = [
  {
  path: '',
  loadChildren: () => import('./modules/home/home.module').then(m => m.HomeModule)
},
{
  path: 'home',
  loadChildren: () => import('./modules/home/home.module').then(m => m.HomeModule),
  data: {
    breadcrumbs: 'Inicio'
  }
},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
